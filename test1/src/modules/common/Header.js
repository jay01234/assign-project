import * as React from "react";
import Headroom from "react-headroom";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import './header.css';
import Gigselect from "./Gigselect";


const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: 200,
    },
  },
 
}));


  export default class Header extends React.Component {
    render() {
    
    
        return (
          
          <React.Fragment>
            <Headroom>
       
                <a className="headerPersonalizeLink"
                style={{color: this.props.isLoggedIn ? '#959797': '#2E81FD', fontWeight: 'bold',fontSize:'12px'}}>
                     I'm Nurse <span  style={{ fontSize: '24px', color: '#C4C4C4', margin: '0px 6px 0px 6px',position:'relative', top: '-2px'}} >.</span> Recruiter?
                 </a>
                 <div>
                 <h1 class="heading">Find your gigs</h1> 
                 <p class="preheading">Please select below options for suitable gigs</p>
                 </div>
                 <div> 
                    <label class="inputFieldLabel">
                        Gig Type (select one or both)
                     </label>
                     <Gigselect />
                 </div>
                 <p class="inputFieldLabel">State Licensed Location OR Any Location</p>
                 <Typography>
          <TextField id="outlined-basic" label=" Central Delhi,Delhi "  variant="outlined" style={{
                margin:'0px 50px 0px 0px',
                padding:'0px 40px 0px 11px'
          }} />
        </Typography>
                 <p class ="locationsp">Enter your licensed location or feel free to explore any location</p>
                 
                 <div class="discipline">
                   <h3 class="title">Discipline/Title</h3>
                   </div>
                   
                   
                   <Typography>
          <TextField id="outlined-basic" label=" Select discipline "  variant="outlined" style={{
                margin:'0px 50px 0px 0px',
                padding:'0px 40px 0px 11px'
          }} />
        </Typography>
        <button class="muibutton1" > Show Offers </button>
                 </Headroom>  
          </React.Fragment>    
        )
    }
}