import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import Headroom from "react-headroom";
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import './header.css'


const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing(3),
  },
}));

export default function Gigselect() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    contract: true,
    permanent: false,
    
  });

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked });
  };

  const {contract,permanent } = state;
  const error = [contract,permanent ].filter(v => v).length !== 2;

  return (
   
      <div className={classes.root}>
         
          
      <FormControl component="fieldset" className={classes.formControl}>
      
        <FormGroup>
          <FormControlLabel
            control={<Checkbox checked={contract} onChange={handleChange('contract')} value="contract" />}
            label="Contract"
          />
         
        </FormGroup>
      </FormControl>
      <FormControl required error={error} component="fieldset" className={classes.formControl}>

        <FormGroup>
          
          <FormControlLabel
            control={<Checkbox checked={permanent} onChange={handleChange('permanent')} value="permanent" />}
            label="Permanent"
          />
         
        </FormGroup>
      </FormControl>
     
       
    </div>
  );
}