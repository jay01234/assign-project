import React, { Component } from 'react';
import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/styles';
import TextField from '@material-ui/core/TextField';
import * as Custom from '../../common/components/CustomModules';
import AutoSuggestLocation from './AutoSuggestLocation';
// import AutoSuggestSpeciality from './AutoSuggestSpeciality';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlightMatch from 'autosuggest-highlight/match';
import AutosuggestHighlightParse from 'autosuggest-highlight/parse';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIosRoundedIcon from '@material-ui/icons/ArrowBackIosRounded';
import Router from 'next/router';
import EjectIcon from '@material-ui/icons/Eject';

import Slide from '@material-ui/core/Slide';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import DisciplineDropDown from './DisciplineDropDown';
import { getFullStateName, getStateCode } from '../../common/utilities';
//import MultiSelectA from './MultiSelectA';
import PublishIcon from '@material-ui/icons/Publish';
import CircularProgress from '@material-ui/core/CircularProgress';
import Input from '@material-ui/core/Input';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import Tooltip from '@material-ui/core/Tooltip';

const styles = {
  loaderCenter: {
    top: '45%',
    left: '0',
    right: '0',
    margin: '0 auto',
  },
  autoSuggestRoot: {
    marginBottom: '20px'
  },
  trackBtn: {
    right: '12px',
    top: '33px',
    border: 'none',
    outline: 'none',
    background: '#FFEFF3',
    padding: '0',
    borderRadius: '6px',
    width: '30px',
    height: '30px',

  },
  closeBtnBg: {
    background: '#fff',
    height: '50px',
    width: '100%',
    marginBottom: '10px'
  },
  closeBtn: {
    top: 15
  },
  customInput: {
    background: '#FFFFFF',
    border: '1px solid #EEEEEE',
    height: '50px',
    borderRadius: '6px',
    fontSize: '14px',
    lineHeight: '50px',
    paddingRight: '10px',
    boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
    color: '#777',
    paddingLeft: '18px',
    display: 'flex',
    alignItems: 'center',

    '&.Mui-focused': {
      background: '#ffffff',
      color: '#777'
    },
    '& input': {
      color: '#777',
      fontSize: '14px',
      lineHeight: '50px',
      background: '#F9FBFC',
      borderRadius: '6px',
      border: '0px',
      '&:focus': {
        background: '#ffffff',
        border: 'none',
        borderRadius: '6px',
        color: '#777'
      }
    },
    '&:after': {
      display: 'none'
    },
    '&:before': {
      display: 'none'
    }
  },

  inputFieldLabel: {
    fontSize: '13px',
    lineHeight: '15px',
    color: '#000000',
    marginBottom: '8px',
    display: 'inline-block',
    width: '100%',
    fontWeight: 550
  },
  innerContainerSpacing: {
    padding: '0px 30px'
  },

  spacing: {
    marginBottom: '18px'
  },
  multiSelect: {
    color: '#999999',
    paddingLeft: '5px',
  },
  supportedText: {
    fontSize: '11px',
    lineHeight: '13px',
    color: '#787878',
    margin: '7px'
  },

  chip: {
    fontSize: '14px',
    lineHeight: '16px',
    color: '#D2357E',
    borderRadius: '6px',
    backgroundColor: '#FFEFF3',
    height: '32px',
    border: '1px solid #F2638B',
    padding: '7px 10px',
    marginRight: '10px',
    marginBottom: '10px',
    display: 'inline-block',
    boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
  },
  loadMoreItems: {
    fontSize: '14px',
    lineHeight: '16px',
    color: '#EA3957',
    '& a': {
      textDecoration: 'none',
      color: '#EA3957',
    }
  },
  backBtn: {
    padding: 0,
    marginBottom: '20px'
  },
  subHeading: {
    fontSize: '31px',
    lineHeight: '40px',
    marginBottom: '0px',
    color: '#2A2A2A',
    marginLeft: '0px',
    padding: '30px 30px 0',
    marginTop: '0px',
    fontWeight: 500import React from 'react';
    import { makeStyles, withStyles } from '@material-ui/core/styles';
    import InputLabel from '@material-ui/core/InputLabel';
    import MenuItem from '@material-ui/core/MenuItem';
    import FormControl from '@material-ui/core/FormControl';
    import Select from '@material-ui/core/Select';
    import NativeSelect from '@material-ui/core/NativeSelect';
    import InputBase from '@material-ui/core/InputBase';
    
    const BootstrapInput = withStyles(theme => ({
      root: {
        'label + &': {
          marginTop: theme.spacing(3),
        },
      },
      input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 16,
        padding: '10px 26px 10px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
          '-apple-system',
          'BlinkMacSystemFont',
          '"Segoe UI"',
          'Roboto',
          '"Helvetica Neue"',
          'Arial',
          'sans-serif',
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"',
        ].join(','),
        '&:focus': {
          borderRadius: 4,
          borderColor: '#80bdff',
          boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
      },
    }))(InputBase);
    
    const useStyles = makeStyles(theme => ({
      margin: {
        margin: theme.spacing(1),
      },
    }));
    
    export default function ReceivedOtp() {
      const classes = useStyles();
      const [age, setAge] = React.useState('');
      const handleChange = event => {
        setAge(event.target.value);
      };
      return (
        <div>
          <FormControl className={classes.margin}>
            <InputLabel htmlFor="demo-customized-textbox">Age</InputLabel>
            <BootstrapInput id="demo-customized-textbox" />
          </FormControl>
          <FormControl className={classes.margin}>
            <InputLabel id="demo-customized-select-label">Age</InputLabel>
            <Select
              labelId="demo-customized-select-label"
              id="demo-customized-select"
              value={age}
              onChange={handleChange}
              input={<BootstrapInput />}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          </FormControl>
          <FormControl className={classes.margin}>
            <InputLabel htmlFor="demo-customized-select-native">Age</InputLabel>
            <NativeSelect
              id="demo-customized-select-native"
              value={age}
              onChange={handleChange}
              input={<BootstrapInput />}
            >
              <option value="" />
              <option value={10}>Ten</option>
              <option value={20}>Twenty</option>
              <option value={30}>Thirty</option>
            </NativeSelect>
          </FormControl>
        </div>
      );
    }
    input': {
      background: '#FFFFFF',
      border: '1px solid #EEEEEE',
      height: '44px',
      borderRadius: '6px',
      fontSize: '14px',
      lineHeight: '44px',
      paddingRight: '10px',
      boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
      color: '#777',
      paddingLeft: '18px',
    },
    
    '& .MuiInput-underline:after': {
      borderBottom: '0px'
    },
    '& .MuiInputBase-input': {
      paddingLeft: '28px',
      color: '#777',
    }

  },
  locationSp: {
    border: '1px solid #EEEEEE',
    boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
    borderRadius: '6px',
    '& .MuiInput-input': {
      background: '#FFFFFF',      
      height: '30px',      
      fontSize: '14px',
      lineHeight: '44px',
      paddingRight: '45px',      
      color: '#777',
      paddingLeft: '44px',
      borderRadius: '6px',
      paddingTop: '7px'
    },
    '& .MuiInput-underline:after': {
      borderBottom: '0px'
    },
  },
  autoSuggestGrid: {
    background: '#FFEFF3',
    boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
    borderRadius: '6px',
    padding: '0px 0 0 9px',
    display: 'inline-block',
    marginBottom: '3px',
    marginRight: '10px',
    fontSize: '10px',
    lineHeight: '14px',
    color: '#D2357E',
    '& .MuiSvgIcon-root': {
      color: '#D2357E'
    }  
  },
};

const renderSpecialityInputComponent = (inputProps: any, len: number) => (
  <Input
    {...inputProps}
    style={{paddingLeft: len === 5 ? '10px' : '18px'}}
  />
);

const renderSpecialitySuggestion = (suggestion: any, { query }) => {
  const matches = AutosuggestHighlightMatch(suggestion.label, query);
  const parts = AutosuggestHighlightParse(suggestion.label, matches);

  return (
    <div className="optionOverflow">
      {parts.map((suggestion, index) => {
        const className = suggestion.highlight ? 'react-autosuggest__suggestion-match' : null;

        return (
          <span className={className} key={index}>
            {suggestion.text}
          </span>
        );
      })}
    </div>
  );
}

const getSpecialitySuggestionValue = (suggestion: any) => {
  return suggestion.label;
}

const shouldRenderSuggestions = (value: any) => {
  return true;
}

const HtmlTooltip = withStyles(() => ({
  tooltip: {
    backgroundColor: '#05BAAF',
    color: '#fff',
    width: '100%',
    fontSize: '12px',
    borderRadius: '8px',
    padding: '10px',
    maxWidth: '315px'
  },
}))(Tooltip);

class SetPreferenceUI extends Component<Props, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      defaultLocation: '',
      defaultProfile: [],
      defaultDiscipline: '',
      isMenuProfileOpen: false,
      isMenuLocationOpen: false,
      showLoader: false,
      selectedOption: ['contract'],
      afterNext: false,
      // isSliding: false,
      loadMoreItem: false,
      desiredLocationCount: 6,
      selectedDesiredLocation: [],
      desiredLocationResults: [],
      noGigsAvailable: false,
      specialityText: '',
      notificationOpen: false,
      locationToolTipOpen: false,
      notificationSpecialityOpen: false,
    };
  }
  componentDidMount() {    
    this.props.resetLocationPreference(false)
    if (this.props.requestCompleted && (this.props.latitude === null || this.props.longitude === null)) {
      this.props.onLocationRequest({type: 'no', isBlockDefault: true});
    }
    if (this.props.requestCompleted && this.props.latitude !== null) {
      this.props.onLocationRequest({type: 'default', lat: this.props.latitude, long: this.props.longitude });
    }
    if (this.props.selectedLocation !== '') {
      this.setState({ defaultLocation: this.props.selectedLocation });
    }
    //const arr = this.props.specialitySelected.map((obj) => obj.value);
    //this.props.onDesiredLocation(100, arr);
    //this.props.onSpecialityRequest({ query: '', discipline: this.props.disciplineSelected });
    if (this.props.selectedLocation !== '') {
      this.setState({ defaultLocation: this.props.selectedLocation });
    }
    if (this.props.resumeName !== '') {
      this.setState({ resumeName: this.props.resumeName, showSuccessMessage: true });
    }
    this.setState({
      defaultDiscipline: this.props.disciplineSelected,
      defaultProfile: this.props.specialitySelected,
      selectedOption: this.props.jobType,
      selectedDesiredLocation: this.props.selectedDesiredLocation,
      //compensation: this.props.compensation,
      desiredLocationResults: this.props.desiredLocationResults,
      locationToolTipOpen: true
    }, () => {
      if (this.state.defaultProfile.length === 5) {
        this.setState({ notificationSpecialityOpen: true })
      }
    });

    if (!sessionStorage.getItem('sgsite')) {
      sessionStorage.setItem('sgsite', 'true');
    }
    if (this.props.authErrorMessage === 'User Logged out') {
      this.setState({ notificationOpen: true });
      this.myCallback();
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.isLoggedIn !== nextProps.isLoggedIn && nextProps.isLoggedIn) {
      this.props.onDisciplineRequest();      
      //this.props.onSpecialityRequest({ query: '', discipline: this.props.disciplineSelected });
      if (!sessionStorage.getItem('sgsite')) {
        sessionStorage.setItem('sgsite', 'true');
      }
      navigator.permissions &&
        navigator.permissions
          .query({ name: 'geolocation' })
          .then(PermissionStatus => {
            if (PermissionStatus.state === 'granted') {
              navigator.geolocation.getCurrentPosition(this.showPosition);
            } else {
              // this.props.onLocationRequest({ type: 'no' });
            }
          });    
    }
    if (this.props.isProfileMenuOpen !== nextProps.isProfileMenuOpen) {
      this.setState({ isMenuProfileOpen: nextProps.isProfileMenuOpen });
    }
    if (this.props.isLocationMenuOpen !== nextProps.isLocationMenuOpen) {
      this.setState({ isMenuLocationOpen: nextProps.isLocationMenuOpen });
    }
    if (this.props.specialitySelected && nextProps.specialitySelected && this.props.specialitySelected.length !== nextProps.specialitySelected.length) {
      //const arr = nextProps.specialitySelected.map((obj) => obj.value);
      //this.props.onDesiredLocation(100, arr);
      this.setState({ defaultProfile: nextProps.specialitySelected }, () => {
        if (this.state.defaultProfile.length === 5) {
          this.setState({ notificationSpecialityOpen: true })
        }
      });
    }
    if (this.props.disciplineSelected !== nextProps.disciplineSelected) {
      this.setState({ defaultDiscipline: nextProps.disciplineSelected });
    }
    if (this.props.selectedLocation !== nextProps.selectedLocation) {
      this.setState({ defaultLocation: nextProps.selectedLocation });
    }
    if (this.props.jobType !== nextProps.jobType) {
      this.setState({ selectedOption: nextProps.jobType });
    }
    if (this.props.requestCompleted !== nextProps.requestCompleted && (nextProps.latitude === null || nextProps.longitude === null)) {
      this.props.onLocationRequest({type: 'no', isBlockDefault: true});
    }
    if (this.props.requestCompleted !== nextProps.requestCompleted && nextProps.latitude !== null && nextProps.longitude !== null) {
      this.props.onLocationRequest({type: 'default', lat: nextProps.latitude, long: nextProps.longitude });
    }
    if (this.props.resumeName !== nextProps.resumeName && nextProps.resumeName !== '') {
      this.setState({ resumeName: nextProps.resumeName, showSuccessMessage: true });
    }
    if (this.props.selectedDesiredLocation && nextProps.selectedDesiredLocation && this.props.selectedDesiredLocation.length !== nextProps.selectedDesiredLocation.length) {
      this.setState({ selectedDesiredLocation: nextProps.selectedDesiredLocation });
    }
    if (this.props.desiredLocationResults.length !== nextProps.desiredLocationResults.length) {
      this.setState({ desiredLocationResults: nextProps.desiredLocationResults });
    }
    if (this.props.profileInfo !== nextProps.profileInfo && Object.keys(nextProps.profileInfo).length === 3 && nextProps.defaultLatitude !== null ) {
      this.props.onLocationRequest({type: 'default', lat: nextProps.defaultLatitude, long: nextProps.defaultLongitude });
    }
    if (this.props.profileInfo !== nextProps.profileInfo && Object.keys(nextProps.profileInfo).length === 3 && nextProps.defaultLatitude === null && nextProps.selectedLocation !== '' ) {
      this.props.onLocationRequest({type: 'no'});
    }
    if (this.props.isGigsLoading !== nextProps.isGigsLoading && nextProps.isGigsLoading === false && nextProps.desiredLocationResults.length) {
      this.props.onToggleIndexView(true);      
    }
    if (this.props.isLocationsLoading !== nextProps.isLocationsLoading && nextProps.isLocationsLoading === false && !nextProps.desiredLocationResults.length) {
      this.setState({ noGigsAvailable: true });
    }
    if (this.props.isLocationsLoading !== nextProps.isLocationsLoading && nextProps.isLocationsLoading === false && nextProps.desiredLocationResults.length) {
      this.props.onToggleIndexView(true);
    }
    if (this.props.authErrorMessage !== nextProps.authErrorMessage && nextProps.authErrorMessage === 'User Logged out') {
      this.setState({ notificationOpen: true });
    }
  }
  handleClose = (event: any, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ notificationOpen: false });
  };

  handleCloseSpecialityNotification = (event: any, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ notificationSpecialityOpen: false });
  };
  showPosition = (position: any) => {
    this.props.onLocationAccessGranted(
      position.coords.latitude,
      position.coords.longitude
    );
  };
  addOverlay = () => {
    let para = document.createElement('div');
    para.classList.add('overlayBg');
    let node = document.createTextNode('');
    para.appendChild(node);
    let element = document.getElementById('outerWrapper');
    element.appendChild(para);
  };

  clickProfileTransition = () => {
    this.setState(
      {
        isMenuProfileOpen: !this.state.isMenuProfileOpen
      },
      () => {
        setTimeout(() => {
          //document.getElementById('close-profile').scrollIntoView();
        }, 300);
        this.props.onSearchProfileViewToggle(this.state.isMenuProfileOpen);
      }
    );
  };
  clickLocationTransition = () => {
    this.setState(
      {
        isMenuLocationOpen: !this.state.isMenuLocationOpen
      },
      () => {
        setTimeout(() => {
          document.getElementById('close-location').scrollIntoView();
        }, 500);
        this.props.onSearchLocationViewToggle(this.state.isMenuLocationOpen);
      }
    );
  };

  radioChange = (type: string) => {
    let arr = this.state.selectedOption;
    if (this.state.selectedOption.includes(type)) {
      arr = arr.filter(e => e !== type);
    } else {
      arr.push(type)
    }
    this.setState({
      selectedOption: arr
    }, () => {
      this.props.onJobTypeSelection(this.state.selectedOption);
      this.myCallback();
    });    
  };

  myCallback = () => {
    this.setState({ noGigsAvailable: false });
  }
  getQueryParam = (item: any) => {
    const arr = [];
    item.length ? item.map((obj: any) => {
      arr.push(obj.value ? obj.value : '');
    }) : null
    return arr;
  }
  savePreference = () => {
    this.props.onSetStart(true);
    const arr = this.props.specialitySelected.map((obj) => obj.value);
    this.props.onDesiredLocation(20, {'query': arr, 'jobType': this.state.selectedOption, 'excludedStates': 'none'});
    const props = this.props;
    const data= {
      "jobType": props.jobType,
      "discipline": props.disciplineSelected,
      "subSpeciality": props.specialitySelected,
      "userLocation": {
        "lat": parseFloat(props.latitude) ,
        "lng": parseFloat(props.longitude),
        "fullLocation": props.fullLocation,
        "display_address": props.selectedLocation
      },
      "documents": props.profileInfo && props.profileInfo.documents && props.profileInfo.documents.resume && props.profileInfo.documents.resume.length ? props.profileInfo.documents : {"resume": []},
      "budget": {
        "min": 0,
        "max": 9000,
        "currency": 'usd'
      },
      "desiredLocations": props.selectedDesiredLocation
    };
    if (props.isLoggedIn) {
      this.props.onUpdatePreferenceRequest(data, this.props.userToken.idToken.jwtToken);
    } else {
      this.props.storeGuestPreference(data);;
    }
    this.props.onGigSearchResultRequest({
      limit: 20,
      page: 1,
      query: this.getQueryParam(this.props.specialitySelected),
      stateCodes: this.props.fullLocation ? this.props.fullLocation.state_code : '',
      jobType: this.props.jobType,
      budget: this.props.compensation,
      token: this.props.userToken,
      lat: this.props.latitude,
      lng: this.props.longitude
    });
  }
  addDesiredLocation = (item: any, isSelected: boolean) => {
    const arr = this.state.selectedDesiredLocation;    
    if (isSelected) {
      const i = arr.findIndex((obj: any) => item.stateCode === obj.stateCode);
      arr.splice(i, 1);
    } else {
      item['stateCode'] = item.stateCode;
      item['state'] = getFullStateName(item.stateCode);
      item['countryCode'] = 'usa';
      item['country'] = 'usa';
      arr.push(item);
    }
    this.setState({ selectedDesiredLocation: arr }, () => this.props.setDesiredLocations(this.state.selectedDesiredLocation));
  }
  isSelected = (label: string) => {
    const output = this.state.selectedDesiredLocation.filter((obj: any) => obj.stateCode === label);
    if (output.length) {
      return true;
    } else {
      return false;
    }
  }
  suggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
    const arr = this.state.defaultProfile;
    const i = arr.findIndex((obj: any) => obj.value === suggestionValue);
    if (i === -1) {
      const obj = {label: suggestionValue, type: 'Profession', value: suggestionValue};
      arr.push(obj);
      this.props.onSpecialitySelection(arr);
    }
    if (arr.length === 5) {
      this.setState({ notificationSpecialityOpen: true });
    }
    this.setState({
      specialityText: ''
    });
  }
  onChangeSpeciality = (event, { newValue }) => {
    this.setState({
      specialityText: newValue
    });
    this.setState({ noGigsAvailable: false });
  };
  removeSpeciality = (e, val) => {
    e.preventDefault();
    const arr = this.state.defaultProfile;
    const i = arr.findIndex((obj: any) => obj.value === val);
    arr.splice(i, 1);
    this.props.onSpecialitySelection(arr);
    this.setState({
      specialityText: ''
    });
    this.setState({ notificationSpecialityOpen: false });
    this.setState({ noGigsAvailable: false });
  }
  onSpecialitySuggestionsClearRequested = () => {
    this.setState({
      profileSuggestions: []
    });
  };
  onSpecialitySuggestionsFetchRequested = ({ value }) => {
    this.props.onSpecialityRequest({ query: value, discipline: this.state.defaultDiscipline });
  }
  specialityClick = () => {
    document.getElementById('specialitySuggest').scrollIntoView();
  }
  handleTooltipClose = () => {
    this.setState({ locationToolTipOpen: false });
  };
  render() {
    const {
      classes,
      disciplineResults,
      disciplineSelected,
      defaultLatitude,
      defaultLongitude,
      specialityResults,
      specialitySelected,
      hash,
      isLoading,
      isLocationsLoading,
      isLoggedIn,
      profileInfo,
      onToggleIndexView,
      isAppStarted
    } = this.props;
    
    const loadMore = [];
    this.state.desiredLocationResults && this.state.desiredLocationResults.map((item, i) => {
      loadMore.push(<a className={classes.chip} style={{ background: this.isSelected(item.stateCode) ? '#FFEFF3' : '#fff', color: this.isSelected(item.stateCode) ? '#D2357E' : '#c1c1c1', border: this.isSelected(item.stateCode) ? '1px solid #F2638B' : '1px solid #C1C1C1' }} key={`${i}_desired`} onClick={() => this.addDesiredLocation(item, this.isSelected(item.stateCode))}>{getFullStateName(item.stateCode)} <span className = "fontWeightBold">({item.gigCount})</span></a>)
    });

    const specialityProps = {
      placeholder: 'Enter Speciality',
      value: this.state.specialityText,
      onChange: this.onChangeSpeciality,
      autoFocus: false,
      id: 'multiSpeciality-select'
    };

    const specialityPropsDisabled = {
      placeholder: 'Enter Speciality',
      value: this.state.specialityText,
      onChange: this.onChangeSpeciality,
      autoFocus: false,
      id: 'multiSpeciality-select-disabled',
      disabled: true
    };

    return (
      <div className="setPreference" style={{ overflowY: 'auto', height: 'calc(100% - 105px)' }}>
        {
          isLoading ?
            <div className={classes.loaderCenter} style={{ position: 'absolute', textAlign: 'center' }}><img src={require('../../../../assets/sg-loading.gif')} alt="loading" /></div> :
            <Grid
              item
              xs={12}
              style={{
                display:
                  this.state.isMenuProfileOpen || this.state.isMenuLocationOpen
                    ? 'none'
                    : 'block'
              }}
            >
              <form autoComplete="off" name={ isLoggedIn ? 'loggedInUserPereferenceForm' : 'guestPereferenceForm'} id={ isLoggedIn ? 'loggedInUser-pereferenceForm' : 'guest-pereferenceForm'}>
    
                {!this.state.afterNext ?
    
                  <React.Fragment>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                      <div>
                        <h2 className={`${classes.subHeading}`}>
                        {
                          !isLoading ?
                            isLoggedIn && profileInfo ?
                            Object.keys(profileInfo).length > 3 ? 'Update Preferences' : 'Set Preferences' : 'Find your gigs'
                          : null
                        }                       
                        </h2>
                        <p className="pref-subHeading">Please select below options for suitable gigs.</p>
                      </div>           
                      {
                        isLoggedIn && profileInfo && Object.keys(profileInfo).length > 3 && isAppStarted ?
                        <IconButton style={{ color: '#787878' }} aria-label="close" color="primary" onClick={() => onToggleIndexView(true)}>
                          <CloseIcon style={{ fontSize: '30px' }} />
                        </IconButton> : null
                      }
                    </div>
                    <div className={`${classes.innerContainerSpacing}`}>
                          
                    <div className={classes.spacing}>
                      <label className={`${classes.inputFieldLabel} fontWeight`}>
                        Gig Type (select one or both)
                     </label>
                        
                     <div className='radioButtonCollection'>
                      <label
                          className={`checkBoxContainer contract ${this.state.selectedOption.includes('contract') ? 'selected' : ''}`}
                          style={{ marginRight: '10px' }}
                        >
                          <input
                            type="checkbox"
                            defaultChecked={this.state.selectedOption.includes('contract')}
                            checked={this.state.selectedOption.includes('contract')}
                            name="checkbox"
                            id="contract-option"
                            onChange={() => this.radioChange('contract')}
                          />
                          <span className="checkmark">Contract</span>
                        </label>
                        <label className={`checkBoxContainer permanent ${this.state.selectedOption.includes('permanent') ? 'selected' : ''}`}>
                          <input
                            type="checkbox"
                            name="checkbox"
                            id="permanent-option"
                            onChange={() => this.radioChange('permanent')}
                            defaultChecked={this.state.selectedOption.includes('permanent')}
                            checked={this.state.selectedOption.includes('permanent')}
                          />
                          <span className="checkmark">Permanent</span>
                        </label>
                      </div>          
                    </div>

                    <div
                        id="autoSuggestLocationContainer"
                        className={`${classes.autoSuggestRoot} ${classes.spacing} locationContainer positionRelative`}
                      >
                      <label className={`${classes.inputFieldLabel} fontWeight`}>
                        State Licensed Location OR Any Location
                      </label>
                      <HtmlTooltip
                        onClose={this.handleTooltipClose}
                        open={this.state.locationToolTipOpen}
                        title={
                          <React.Fragment>
                            <EjectIcon className="tooltip-arrow" />
                            <div style={{ fontWeight: 'bold', marginBottom: '3px' }}>Welcome to {this.state.defaultLocation}!</div>
                            <div> We found your current location to continue or you can enter your desired location</div>
                          </React.Fragment>
                        }
                      >       
                        <div id="locationConatainer" className={classes.locationSp}>
                          <AutoSuggestLocation
                            type={'location'}
                            defaultLocation={this.state.defaultLocation}
                          />
                        </div>
                      </HtmlTooltip>
                      <span className="location_sp_icon" />
                      <button
                        className={`${classes.trackBtn} positionAbsolute`}
                        onClick={e => {
                          e.preventDefault();
                          let params: any = {
                            lat: defaultLatitude,
                            long: defaultLongitude,
                            type: 'default'
                          };
                          if (defaultLatitude === null || defaultLongitude === null) {
                            params = { type: 'no' };
                          }
                          this.props.onLocationRequest(params);
                        }}
                      >
                        <span className="trackingBtn_sp" />
                      </button>
                      <p className="pref-location-subDesc">Enter your licensed location or feel free to explore any location</p>
                    </div>
    
                    <div className={`desciplineContainer ${classes.spacing}`}>
                      <label className={`${classes.inputFieldLabel} fontWeight`}>
                        Discipline/Title
                  </label>
                      <DisciplineDropDown
                        options={disciplineResults}
                        onDisciplineSelection={this.props.onDisciplineSelection}
                        onSpecialitySelection={this.props.onSpecialitySelection}
                        selectedDiscipline={this.state.defaultDiscipline}
                        callbackFromParent={this.myCallback}
                      />
                    </div>
    
                    <div
                      id="autoSuggestSpecialityContainer"
                      className={`${classes.autoSuggestRoot} ${classes.spacing} specialityContainer`}
                      style={{ display: this.props.disciplineSelected !== '' ? 'block' : 'none' }}                     
                    >
                      <label className={`${classes.inputFieldLabel} fontWeight`}>
                        Speciality/Skills <span style={{ fontSize: '12px', color: '#555' }}>(upto 5)</span>
                     </label>                                            
                      <div>

                      {/*<MultiSelectA
                        options={specialityResults}
                        onSpecialitySelection={this.props.onSpecialitySelection}
                        specialitySelected={specialitySelected}
                        callbackFromParent={this.myCallback}
                      />*/}
                      <div id="specialitySuggest" className = {`${classes.specialitySp}`} onClick={this.specialityClick} style={{ position: 'relative' }}>
                        <Autosuggest
                          suggestions={specialityResults}
                          onSuggestionsFetchRequested={this.onSpecialitySuggestionsFetchRequested}
                          onSuggestionsClearRequested={this.onSpecialitySuggestionsClearRequested}
                          getSuggestionValue={getSpecialitySuggestionValue}
                          renderSuggestion={renderSpecialitySuggestion}
                          inputProps={this.state.defaultProfile.length === 5 ? specialityPropsDisabled : specialityProps}
                          renderInputComponent={(item) => renderSpecialityInputComponent(item, this.state.defaultProfile.length)}
                          onSuggestionSelected={(event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => this.suggestionSelected(event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method })}
                          shouldRenderSuggestions={shouldRenderSuggestions}
                        />
                        {/*<button className="specialityClose" onClick={(e) => {e.preventDefault()}} />*/}
                      </div>  
                      {/*<AutoSuggestSpeciality />*/}
                      <div style={{marginTop: '10px'}}>
                      {
                        this.state.defaultProfile.length ? this.state.defaultProfile.map((obj, i) =>
                        <span className = {`${classes.autoSuggestGrid}`} key={`${i}_speciality`}>
                          {obj.value}
                          <IconButton style={{ padding: '5px' }} aria-label="remove" onClick={(e) => this.removeSpeciality(e, obj.value)}>
                            <CloseIcon style={{ fontSize: '14px' }} />
                          </IconButton>
                        </span>
                        ) : null
                      }
                     </div> 
                      </div>
             
                    </div>
                       
                  </div>
                    {
                      this.state.noGigsAvailable ?
                        <p className={`${classes.supportedText} textAlign`} style={{ color: '#ff0000', fontSize: '11px', lineHeight: '1', margin: '0 0 0 31px', textAlign: 'left' }}>
                          No gigs available, please change your selected preferences.
                        </p> : null
                    }
                    {
                      this.props.disciplineSelected !== '' && this.props.specialitySelected && this.props.specialitySelected.length && this.props.selectedLocation !== '' && !this.state.noGigsAvailable ?
                      isLocationsLoading ?
                          <p style={{ textAlign: 'center', position: 'absolute', bottom: '35px', left: '0', right: '0', margin: '0 auto' }}>
                            <CircularProgress style={{ color: '#333' }} />
                          </p> :
                            <Custom.ButtonPrimary
                              variant="contained"
                              color="primary"
                              fullWidth={true}
                              style={{fontWeight: 'bold', position: 'absolute', bottom: '35px', width: '80%', left: '0', right: '0', margin: '0 auto'}}
                              onClick={this.savePreference}
                            >
                               Show Matched Gigs
                            </Custom.ButtonPrimary> :
                              <Custom.ButtonSecondary
                                variant="contained"
                                color="primary"
                                fullWidth={true}
                                style={{fontWeight: 'bold', position: 'absolute', bottom: '35px', width: '80%', left: '0', right: '0', margin: '0 auto'}}
                                disabled
                              >
                                Show Matched Gigs
                              </Custom.ButtonSecondary>
                    }
                  </React.Fragment>
                  : null
                }
   
              </form>
            </Grid>
        }
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
          open={this.state.notificationOpen}
          autoHideDuration={2000}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{'You have been logged out successfully'}</span>}
        />
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
          className="speciality-alert"
          open={this.state.notificationSpecialityOpen && !this.props.currentIndexResultsView}
          autoHideDuration={3000}
          onClose={this.handleCloseSpecialityNotification}
          ContentProps={{
            'aria-describedby': 'alert-msg',
          }}
          action={[
            <IconButton
              key="close"
              aria-label="close"
              color="inherit"
              className="alert-close"
              onClick={this.handleCloseSpecialityNotification}
            >
              <CloseIcon />
            </IconButton>
          ]}
          message={<span className="speciality-alert-msg" id="alert-msg">
            <div style={{ marginBottom: '3px' }}>Alert!</div>
            <div>You can only select 5 speciality/skills.</div>
          </span>}
        />       
      </div>
    );
  }
}

const mapStateToProps = (state: any, ownProps: any) => ({
  latitude: state.guest.userLocation.latitude,
  longitude: state.guest.userLocation.longitude,
  defaultLatitude: state.guest.userLocation.defaultLatitude,
  defaultLongitude: state.guest.userLocation.defaultLongitude,
  location: state.guest.userLocation.location,
  isProfileMenuOpen: state.guest.userLocation.isProfileMenuOpen,
  isLocationMenuOpen: state.guest.userLocation.isLocationMenuOpen,
  selectedLocation: state.guest.userLocation.selectedLocation,
  isAppStarted: state.guest.auth.isAppStarted,
  disciplineResults: state.guest.preference.discipline.results,
  disciplineSelected: state.guest.preference.discipline.selected,
  specialityResults: state.guest.preference.speciality.results,
  specialitySelected: state.guest.preference.speciality.selected,
  hash: state.router.location.hash,
  isLoggedIn: state.guest.auth.isLoggedIn,
  profileInfo: state.guest.auth.profileInfo,
  userToken: state.guest.auth.userToken,
  jobType: state.guest.preference.jobType,
  desiredLocationResults: state.guest.preference.desiredLocation.results,
  fullLocation: state.guest.userLocation.fullLocation,
  selectedDesiredLocation: state.guest.preference.desiredLocation.selected,
  requestCompleted: state.guest.userLocation.requestCompleted,
  resumeName: state.guest.preference.resumeName,
  loginFailure: state.guest.auth.loginFailure,
  isLoading: state.guest.preference.isLoading,
  isLocationsLoading: state.guest.preference.desiredLocation.isLocationsLoading,
  authErrorMessage: state.guest.auth.errorMessage,
  compensation: state.guest.preference.compensation,
  isGigsLoading: state.guest.gigListing.isLoading,
  currentIndexResultsView: state.guest.auth.currentIndexResultsView,
});
const mapDispatchToProps = (dispatch: any) => ({
  onLocationRequest: (query: any) => dispatch(onLocationRequest(query)),
  onLocationAccessGranted: (latitude: any, longitude: any) =>
    dispatch(onLocationAccessGranted(latitude, longitude)),
  onDisciplineRequest: () => dispatch(onDisciplineRequest()),
  onDisciplineSelection: (disciplne: any) => dispatch(onDisciplineSelection(disciplne)),
  onSearchProfileViewToggle: (isProfileMenuOpen: boolean) =>
    dispatch(onSearchProfileViewToggle(isProfileMenuOpen)),
  onSearchLocationViewToggle: (isLocationMenuOpen: boolean) =>
    dispatch(onSearchLocationViewToggle(isLocationMenuOpen)),
  onSpecialityRequest: (query: string) =>
    dispatch(onSpecialityRequest(query)),
  onSetStart: (flag: boolean) => dispatch(onSetStart(flag)),
  onSpecialitySelection: (speciality: any) =>
    dispatch(onSpecialitySelection(speciality)),
  onUpdatePreferenceRequest: (query: any, token: any) => dispatch(onUpdatePreferenceRequest(query, token)),
  onJobTypeSelection: (jobType: string) => dispatch(onJobTypeSelection(jobType)),
  onDesiredLocation: (type: number, query: any) => dispatch(onDesiredLocationRequest(type, query)),
  setDesiredLocations: (locations: any) => dispatch(setDesiredLocations(locations)),
  //setDesiredCompensation: (compensation: number) => dispatch(setDesiredCompensation(compensation))
  storeGuestPreference: (data: any) => dispatch(storeGuestPreference(data)),
  onToggleIndexView: (showResults: boolean) => dispatch(onToggleIndexView(showResults)),
  resetLocationPreference: (flag: boolean) => dispatch(resetLocationPreference(flag)),
  onGigSearchResultRequest: (query: any) => dispatch(onGigSearchResultRequest(query))
});
export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(SetPreferenceUI) as any);