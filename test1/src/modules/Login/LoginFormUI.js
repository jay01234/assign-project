import * as React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
          margin: theme.spacing(1),
          width: 200,
        },
      },
  card: {
    minWidth: 275,
    borderRadius:'35px'
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 18
  },
  pos: {
    marginBottom: 12,
  },
  
}));

export default function LoginFormUI() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} color="#000000" gutterBottom>
          Start With Your Mobile Number<br/><br/>
        </Typography>
        <Typography fontSize="14px" color="#333333">
        Whether you're creating an account or <br/>
        Signing back in<br/><br/>
        </Typography>
        
        <Typography style={{
            float:'left'
        }}>
          Mobile Number
          </Typography>
          <Typography>
          <TextField id="outlined-basic" label="+91   You will get the OTP " variant="outlined" style={{
                margin:'0px 50px 0px 0px',
                padding:'0px 40px 0px 11px'
          }} />
        </Typography>
        <Typography>
            <p style={{fontSize:'12px',float:'left',margin:'0px'}}>By logging in, you agree to the following Policy & Terms of use </p>
        </Typography>
        <CardActions>
        <Button className={classes.otp} style={{background: 'linear-gradient( #E45D5A 10.29%, #A13AAD 88.89%)'}}>Send OTP</Button>
      </CardActions>
      </CardContent>
      
    </Card>
  );
}