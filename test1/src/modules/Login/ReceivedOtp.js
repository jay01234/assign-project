import React from 'react';
import './ReceivedOtp.css';

export default class ReceivedOtp extends React.Component {
    render () {
        return (
            <div> 
                <div class="header" style={{
                    marginTop:'35px'
                }}>
                    <span style={{
                       marginRight:'20px',
                       color:'rgb(34,34,34)'
                    }}>+91</span>
                    <span style={{
                        color:'rgb(34,34,34)'
                    }}>9782112394</span>
                    <button class="bttn" style={{ 
                        float:'right'
                    }}>Edit</button>

                </div>
                 <p>One Time Password </p>
                <div className="InputOtpCollection" style={{
                    display:'flex'
                }}>
                <div class="otpContainer" >
                    
            <input type="number"
              
              name="otpValue1"
              id="one"
              
              style={{ fontWeight: 550, textAlign: 'center', boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
            fontSize: '30px',LineHeight: '35px',
            width:'50px',
            height: '44px',
            border: '0',
            borderRadius:'6px'
            }}
           
              maxLength={1}
         
              
            />
            </div>
            <div class="otpContainer"  >
                    
            <input type="number"
              
              name="otpValue2"
              id="one"
              
              style={{ fontWeight: 550, textAlign: 'center',boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
              fontSize: '30px',LineHeight: '35px',
              width:'50px',
              height: '44px',
              border: '0',
              borderRadius:'6px' }}
           
              maxLength={1}
         
              
            />
            </div>
            <div class="otpContainer"  >
                    
            <input type="number"
              
              name="otpValue3"
              id="one"
              
              style={{ fontWeight: 550, textAlign: 'center',boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
              fontSize: '30px',LineHeight: '35px',
              width:'50px',
              height: '44px',
              border: '0',
              borderRadius:'6px' }}
           
              maxLength={1}
         
              
            />
            </div>
            <div class="otpContainer"  >
                    
            <input type="number"
              
              name="otpValue4"
              id="one"
              
              style={{ fontWeight: 550, textAlign: 'center',boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.1)',
              fontSize: '30px',LineHeight: '35px',
              width:'50px',
              height: '44px',
              border: '0',
              borderRadius:'6px' }}
           
              maxLength={1}
         
              
            />
            </div>
            </div>
            <p>Resend Otp</p>
            <button> Verify & Signin</button>
            </div>
        )
    }
}