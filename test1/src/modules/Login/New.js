import React, { Component } from 'react';
import './New.css'

export default class New extends React.Component {
    render() {
        return (
            <div className="container" 
             >
                <div className="bg-image">
                    <img src="https://res.cloudinary.com/skillgigs/image/upload/dpr_3.0,f_auto,g_center,q_auto/v1/pwa/Locations/Arizona.jpg" style={{
                          width: '100%',
                          height: '480px',
                          paddingTop: '0px',   
                          backgroundSize:'contain',
                    }}/>
                    <button class = "btn" style={{
                          position: 'absolute',
                          top: '3%',
                          right: '34%',
                          transform: 'translate(-50%, -50%)',
                          borderTopLeftRadius: '0px',
                          borderBottomLeftRadius: '0px',
                          borderRadius: '0px 25px 25px 0px',
                          lineHeight: '28px'
                    }}>Offers</button>
                     <button class = "bttn" style={{
                          position: 'absolute',
                          top: '3%',
                          left: '38%',
                          transform: 'translate(-50%, -50%)',
                          borderBottomRightRadius: '0px',
                          borderToprightRadius: '0px',
                          borderRadius: '25px 0px 0px 25px',
                          backgroundColor: '#D2357E',
                          color:'#FFFFFF',
                          lineHeight:'28px'
                    }}>History</button>
                
                <div className="cardFooter">
                <div className="content" style={{
                    padding:'0px,15px'
                }}>
                    <a classNmae="tapOverplay">
                       <div className="footer" style={{
                           textAlign:'center'
                       }}>
                           <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjIiIGhlaWdodD0iMTEiIHZpZXdCb3g9IjAgMCAyMiAxMSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTEgOS4zMzMzM0wxMSAyTDIxIDkuMzMzMzMiIHN0cm9rZT0iI0QyMzU3RSIgc3Ryb2tlLXdpZHRoPSIzIi8+Cjwvc3ZnPgo=" alt="drag"/>
                           <div> TAP TO SEE DESCRIPTION </div>
                    </div> 
                    <div className="jobStart">
                      Expires Soon                        
                    </div>
                    <h1 className="result">
                        "Nursing"
                        "/"
                        "ACU"
                    </h1>
                    <h2 className="jobName">
                        AZ Med/Surg Flexible Night/Day Shift
                    </h2>
                    
                    </a>
                    </div>
                    <div className="result-pay-container">
                                    <div className="result-pay-average">
                                      Average Pay<br/>
                                      "$ 30/ hr"
                                        
                                    </div>
                                    <div className="result-pay-offer">
                                      <span className="result-pay-offer-inside1">Offered Pay<br/>
                                        "$ 40000/ Week"
                                      </span>
                                      <span className="result-pay-offer-inside2">
                                        <ul className="result-pay-ul">
                                          <li className="result-pay-li">
                                            <img src={require('../../svg/license.svg')} alt="license" />
                                            <div className="small-desc">License</div>
                                          </li>
                                          <li className="result-pay-li">
                                            <img src={require('../../svg/stay.svg')} alt="stay" />
                                            <div className="small-desc">Stay</div>
                                          </li>
                                          <li className="result-pay-li">
                                            <img src={require('../../svg/taxFree.svg')} alt="taxFree" />
                                            <div className="small-desc">Tax Free</div>
                                          </li>
                                          <li className="result-pay-li">
                                            <img src={require('../../svg/travel.svg')} alt="travel" />
                                            <div className="small-desc">Travel</div>
                                          </li>
                                        </ul>
                                      </span>
                                    </div>
                                  </div>
                                  <div className="content">
                                  <div className="classes.iconContainer">
                                    <img src={require('../../svg/medical.svg')} alt="medical" />
                                    <span className="iconTextFacility">
                                      Tuba City Hospital
                                    </span>
                                  </div>
                                  <div className="result-">
                                    <span style={{ fontSize: '11px' }}>3.0</span>
                                    <ul className="result-star-ul">
                                        <li className="result-star-li">
                                          <img src={require('../../svg/orangeStar.svg')} alt="star" />
                                        </li>
                                        <li className="result-star-li">
                                          <img src={require('../../svg/orangeStar.svg')} alt="star" />
                                        </li>
                                        <li className="result-star-li">
                                          <img src={require('../../svg/orangeStar.svg')} alt="star" />
                                        </li>
                                        <li className="result-star-li">
                                          <img src={require('../../svg/grayStar.svg')} alt="star" />
                                        </li>
                                        <li className="result-star-li">
                                          <img src={require('../../svg/grayStar.svg')} alt="star" />
                                        </li>
                                      </ul>
                                  </div>
                                  <div className="result-">
                                    <span className="result-facilityName">Tuba City, Arizona</span>
                                   
                                        <span className="result-miles">
                                           57 miles
                                          </span>
                                          </div>
                                
                                  
                                
                                <img src={require('../../svg/experience.svg')} alt="experience" />
                                <span className="year" style={{
                                    fontSize:'10px'
                                }}> 3 Years</span>
                                <div className="miles" style={{
                                    float:'left'
                                }}>
                                <img src={require('../../svg/clock.svg')} alt="clock" />
                                <span className="years" style={{
                                    fontSize:'10px'
                                }}> 13 Years,Contract</span>
                                <p className="shift" style={{
                                    fontSize:'11px'
                                }}>Shift Info: Night shift 11.5 hr/shift</p>
                                </div>
                                </div>
                                </div>
                                </div>     
            </div>
        )
    }
}