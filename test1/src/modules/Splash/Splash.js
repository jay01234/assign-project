import React, { Component } from 'react';
import Header from '../common/Header'
import './splash.css';

export default class Splash extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showComponent: false,
        };
        this.onClick= this.onClick.bind(this);
    }
    onClick() {
        this.setState({
          showComponent: true,
        });
    }
        
    render () {
        return(
            <div class="heading">
                <h1 class="SplashHeading "> Enable Location Services </h1>
                
                <p class="AllowSubHeading"> Swipefox needs to know your location in order to suggest nearby Gigs. </p>
                <button class="muibutton" onClick={this.onClick}> Allow </button>
                {this.state.showComponent ?
           <Header/> :
           null
        }
            </div>
        )
    }
}