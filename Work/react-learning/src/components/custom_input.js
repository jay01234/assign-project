import React from 'react';
import { Input, Form} from 'antd';

class CustomInput extends React.Component{
  constructor(props){
    super(props);
    this.onChange = this.onChange.bind(this);
    this.state = {
      value: ""
    }
  }
  onChange(e) {
    this.setState({value:e.target.value},()=> {

  })
    
  }
render() {
  return (
    <div>
    <Form.Item label={this.props.label}>
     <Input placeholder={this.props.placeholder}
     value={this.state.value} onChange={ e => this.onChange(e)}/>  
     </Form.Item>
     </div>
  )
}
}
export default CustomInput;