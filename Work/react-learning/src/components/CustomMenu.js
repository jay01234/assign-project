import React,{Component} from 'react';
import {  Icon, Button,Sider } from 'antd';
import { Avatar,Badge } from 'antd';
import './CustomMenu.css';

class CustomMenu extends Component {
    state = {
        collapsed: false,
    };
    render() {
        return (
            <div className = "rightMenu" width="350px">
                    <ul>
                        <li > 
                        
                        <Badge count={11} style={{
                            padding: '0px 8px 0px 8px',
                            margin: '-5px 0px 0px 7px'
                        }}>
                        <Icon type="bell" size="large" />
                        </Badge> 
                         
                        </li>

                        <li >
                        <Avatar style={{ backgroundColor: '#87d068' }} size="small" icon="user" />  
                        </li>
                        <li>
                            admin@skillgigs.com
                        </li>
                    </ul>
                    
            </div>
           
        );
    }
}
export default CustomMenu;