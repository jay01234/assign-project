import React from 'react';
import { Input } from 'antd';
import 'antd/dist/antd.css';

class CustomInput extends React.Component {
    render() {
        const { Search } = Input;
        return (
            <Search
                placeholder="Search Talent Name"
                onSearch={value => console.log(value)}
                style=
                {{
                    width: 250
                }}
            />
        )
    }
}

export default CustomInput;
