import React, { Component } from 'react';
import CustomInput from './custom_input';
import { Form, Button } from 'antd';

export default class App extends Component {

render() {
    return (
        <div>
        <Form name ="form">
        <CustomInput label="Name" placeholder="eg: John Doe"
           name = "name" 
        />
        <CustomInput label="Employee Code" placeholder=" eg: 0001"
         name = "employeeCode"
         />
        <CustomInput label="Email" placeholder="eg: johndoe@example.com"
         name = "email"
         />
    
        </Form>
        <Button>Submit</Button>
      </div>

    )
    }
}

    
