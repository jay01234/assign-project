import React,{Component} from 'react';
import { PageHeader } from 'antd';
import 'antd/dist/antd.css';
const routes = [
    {
        path: 'index',
        breadcrumbName: 'Home',
    },
    {
        path: 'first',
        breadcrumbName: 'Lorem Ipsume',
    },
];
export default class CustomPageHeader extends Component {
    render() {
        return (
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                }}
                title="Lorem ipsume"
                breadcrumb={{routes}}
            />
        );
    }
}