/*import React from 'react';

class App extends React.Component {
    render() {
        return <input type="text" defaultValue="Fourth" ref={this.props.fourth} />;

    }
}

export default class Ref extends React.Component {
    third = React.createRef();
    fourth = React.createRef();

    
    handleSubmit = e => {
        e.preventDefaut();

        console.log(this.refs.first,this.second,this.third.value,this.fourth.value);
        
    };
    render() {
        return (
            <form onSubmit={e => this.handleSubmit(e)}>
                <input type="text" default value="First" ref="first" />
                <input type="text" default value="Second" ref={input =>(this.second=input)}/>
                <input type="text" default value="Third" ref = {this.third}/>

                <App fourth={this.fourth}/>
                <button>Submit</button>
            </form>
        );
    }
}*/

import React from 'react';

export default class Ref extends React.Component {
    onClick = () => {
        console.log(this.firstName,this.lastName,this.Age);
    }

    onKeyUp = (target,e) => {
        if(e.keyCode === 13){
            switch (target) {
                case 'firstName':
                    this.lastName.focus();
                    break;
                case 'lastName':
                    this.Age.focus();
                    break;
                case 'age':
                    this.submit.focus();
                default:
                    this.firstName.focus();
            }
        }
    }
    render() {
        
        return (
            <div clssName="App">
                <div>
                    <span>First Name:</span>
                    <input ref={(input)=>{this.firstName=input}} 
                    onKeyUp = {this.onKeyUp.bind(this,'firstName')}
                    type ="text"/>
                </div>
                <div>
                    <span>Last Name:</span>
                    <input ref={(input)=>{this.lastName=input}}
                    onKeyUp = {this.onKeyUp.bind(this,'lastName')}
                    type ="text"/>
                </div>           
                <div>
                    <span>Age</span>
                    <input ref={(input)=>{this.Age=input}} 
                    onKeyUp = {this.onKeyUp.bind(this,'Age')}
                    type ="text"/>
                </div>
                <div>
                    <input type="submit" value="submit" ref={(input)=>{this.submit=input}}  onClick={this.onClick}/>
                </div>
                
                
            </div>
        )
    }
}