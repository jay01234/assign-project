import React,{Component} from 'react';
import { Layout, Menu, Icon } from 'antd';
import CustomMenu from './CustomMenu';
import CustomPageHeader from './CustomPageHeader';
import CustomInput from './CustomInput';
import CustomDashboard from './CustomDashboard';
import CustomTable from './CustomTable';
import 'antd/dist/antd.css';
import './CustomLayout.css';


const { Header, Sider, Content,Footer } = Layout;

class SiderDemo extends React.Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <Layout>
        
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
        <h2 className="head"><b>ADMIN</b></h2>
          <div className="logo" />
         <img src = "https://files.slack.com/files-pri/T03JRAKUE-FRJFCH3B2/iqtech.jpg" style={{width:'100%'}}/>
          <Menu  mode="inline" defaultSelectedKeys={['1']}>
          
          
            <Menu.Item key="1">
              <Icon type="user" />
              <span>Lorem ispume</span>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="video-camera" />
              <span>Lorem ispume</span>
            </Menu.Item>
            <Menu.Item key="3">
            <Icon type="check-circle" size="small" />
              <span>Lorem ispume</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
            
            <CustomMenu/>
            <CustomPageHeader/>
            <CustomDashboard/>
            
          </Header>
        
          <Content
            style={{
              margin: '32px 24px',
              padding: '160px 0px 50px 10px',
              background: '#fff',
              minHeight: 300,
            }}
          >
              <CustomInput/>
              <CustomTable/>
          </Content>
         
            
          <Footer style={{ textAlign: 'center' }}>© SwipeFox. All Rights Reserved</Footer>
        </Layout>
      </Layout>
    );
  }
}
export default SiderDemo; 