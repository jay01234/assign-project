import React,{Component} from 'react';
import { PageHeader, Button , Modal , Select } from 'antd';
import 'antd/dist/antd.css';
import './CustomDashboard.css';

const { Option } = Select;

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

function handleChange(value) {
  console.log(`Selected: ${value}`);
}

export default class CustomDashboard extends Component {
  state = {
    loading: false,
    visible: false,
    size: 'default'
  };

  handleSizeChange = e => {
    this.setState({ size: e.target.value });
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  };
    render() {
      const { visible, loading, size } = this.state;
        return (
          <div>
    <PageHeader
    style={{
        backgroundColor: '#f0f2f5',
        padding: 24,
      }}
      
      title="Talent Assignment Dashboard"
      extra={[
        <Button type="primary" onClick={this.showModal}>
          + Add Talent Scout
        </Button>,
      ]}/>
      <Modal
      visible={visible}
      title="Add Talent Scout"
      onOk={this.handleOk}
      footer={[
        <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
          Submit
        </Button>,
      ]}
    >
      <div className="formMenu">
      
        Name
        <input label="name"  placeholder = "eg: John Doe" style={{
          margin:'0px 0px 0px 90px',
          padding:'4px 62px 5px 15px',
          borderRadius:'5px'
        }}/><br/><br/>
        Employee Code
        <input label="employeeCode" placeholder = "eg: 0001" style={{
          margin:'0px 0px 0px 25px',
          padding:'4px 62px 5px 15px',
          borderRadius:'5px'
         
        
        }}/><br/><br/>
        Email
        <input label="email" placeholder = "eg: johndoe@example.com" style={{
          margin:'0px 0px 0px 90px',
          padding:'4px 62px 5px 15px',
          borderRadius:'5px'
         
        }}/><br/><br/>
        Role Type
        <Select size={size} defaultValue="Select" onChange={handleChange} style={{ width: 250,
        margin: '0px 0px 3px 60px',
        padding: '0px 0px 12px 0px',
        borderRadius:'0px'  }}>
          {children}
        </Select>
      
      </div>
     
    </Modal>
   
   </div>
  
   );
 }
} 
