import React from 'react';
import { Table,Button} from 'antd';
import 'antd/dist/antd.css';


class CustomTable extends React.Component {
    render () {
const columns = [
    {
        title: <b>Talent Name</b>,
        dataIndex: 'talentName',
        key: 'talentName ',

    },
    {
        title: <b>Start Date</b>,
        dataIndex: 'startDate',
        key: 'startDate',
        
    },
    {
        title: <b>Assign</b>,
        dataIndex: '',
        key: 'x',
        render: () => <a>Leonardo Payne</a>,
      },
    
    {
        title: '',
        dataIndex: 'tag',
        key: '',
    },
];
const data = [];

for (let i=0;i<24;i++){
    data.push({
        key:i,
        talentName:'John Brown',
        startDate:'10/15/2019',
        Assign:'Leonardo Payne',
        tag:<Button type="primary">Remove</Button>
    })
}
      return (
          <div>
              <Table columns={columns} dataSource={data}/>
          </div>
      )
      }
    }
    function onChange(pagination,extra) {
    console.log('params',pagination,extra)
}

export default CustomTable;